//
//  Device.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/27/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import Foundation
import CoreBluetooth

enum DeviceStatus : String {
    case discovered   = "discovered"
    case connected    = "connected"
    case disconnected = "disconnected"
}

class Device : NSObject, NSCoding {
    
    var name : String!
    var status : DeviceStatus = .disconnected
    var serviceUUID: String!
    
    init(name: String, status: DeviceStatus, uuid: String) {
        self.name = name
        self.status = status
        self.serviceUUID = uuid
    }
    
    required convenience init?(coder decoder: NSCoder) {
        guard let name = decoder.decodeObject(forKey: "name") as? String,
              let status = DeviceStatus(rawValue: decoder.decodeObject(forKey: "status") as! String) ,
              let serviceUUID = decoder.decodeObject(forKey: "serviceUUID") as? String
            
        else { return nil }
        
        self.init(name: name, status: status, uuid: serviceUUID)
    }
    
    static func deviceFromPeripheral(peripheral: CBPeripheral) -> Device {
        
        let name = peripheral.name ?? "Unnamed Device"
        let uuid = peripheral.identifier
        var status : DeviceStatus = .disconnected
        
        switch peripheral.state {
            case .connected:
                status = .connected
                break
            case .disconnected:
                status = .disconnected
                break
            case .connecting:
                break
            case .disconnecting:
                break
        }
        return Device(name: name, status: status, uuid: "\(uuid)")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: "name")
        aCoder.encode(self.status.rawValue, forKey: "status")
        aCoder.encode(self.serviceUUID, forKey: "serviceUUID")
    }
}

