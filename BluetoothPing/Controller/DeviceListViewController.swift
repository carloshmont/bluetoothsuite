//
//  DeviceListViewController.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/27/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit
import CoreBluetooth

class DeviceListViewController: UIViewController {
    
    @IBOutlet fileprivate weak var discoveredDevicesTableView: UITableView!
    @IBOutlet fileprivate weak var searchBarButtonItem: UIBarButtonItem!
    
    fileprivate let tableStructure = [TableStructure.connectedDevices, .availableDevices]
    
    fileprivate var connectedPeripherals = [CBPeripheral]()
    fileprivate var foundPeripherals = [CBPeripheral]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        registerBluetoothNotifications()
        discoverPeripherals(withType: .discovery)
    }
    
    deinit {
        deregisterBluetoothNotifications()
    }

    // Navigation bar buttons
    
    @IBAction func closeBarButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchBarButtonTapped(_ sender: UIBarButtonItem) {
        discoverPeripherals(withType: .discovery)
    }
    
}

// Register to bluetooth notifications

extension DeviceListViewController {
    
    func registerBluetoothNotifications() {
        //NotificationCenter.default.addObserver(self, selector: #selector(DeviceListViewController.didTurnOnBluetooth), name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothTurnedOn), object: nil)
    }
    
    func deregisterBluetoothNotifications() {
        //NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothTurnedOn), object: nil)
    }

}

// Discovered Devices Table View DataSource

extension DeviceListViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableStructure.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let thisSection = tableStructure[section]
        switch thisSection {
        case .connectedDevices:
            return connectedPeripherals.count
        case .availableDevices:
            return foundPeripherals.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return createDeviceStatusHeaderTableViewCell(for: section)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let thisSection = tableStructure[section]
        switch thisSection {
            case .connectedDevices:
                if connectedPeripherals.isEmpty {
                    return 0.0
                } else {
                    return 30
                }
            case .availableDevices:
                if foundPeripherals.isEmpty {
                    return 0.0
                } else {
                    return 30
                }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let thisSection = tableStructure[indexPath.section]
        
        switch thisSection {
            case .connectedDevices:
                return createDeviceTableViewCell(for: indexPath, andElement: connectedPeripherals[indexPath.row])
            case .availableDevices:
                return createDeviceTableViewCell(for: indexPath, andElement: foundPeripherals[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let thisSection = tableStructure[section]
        switch thisSection {
            case .connectedDevices:
                return "Connected Devices"
            case .availableDevices:
                return "Available Devices"
        }
    }
    
}

extension DeviceListViewController {
    
    fileprivate func createDeviceTableViewCell(for indexPath: IndexPath, andElement element: CBPeripheral) -> DeviceTableViewCell {
        let thisCell = discoveredDevicesTableView.dequeueReusableCell(withIdentifier: "discoveredDeviceCell", for: indexPath) as! DeviceTableViewCell
        thisCell.deviceName = element.name
        thisCell.uuid = "UUID: \(element.identifier)"
        thisCell.hideSlider = true
        return thisCell
    }
    
    fileprivate func createDeviceStatusHeaderTableViewCell(for section: Int) -> DeviceStatusHeader {
        let thisHeader = discoveredDevicesTableView.dequeueReusableCell(withIdentifier: "deviceStatusHeader") as! DeviceStatusHeader
        let section = tableStructure[section]
        
        switch section {
        case .availableDevices:
            thisHeader.deviceStatus = "Discovered Devices"
            break
        case .connectedDevices:
            thisHeader.deviceStatus = "Connected Devices"
            break
        }
        return thisHeader
    }
}

// Discovered Devices Table View Delegate

extension DeviceListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? DeviceTableViewCell {
            presentDeviceActionMenu(forSender: cell)
        }
    }
    
}

// Initialization Methods

extension DeviceListViewController {
    
    fileprivate enum TableStructure {
        case connectedDevices
        case availableDevices
    }
    
    fileprivate func initialSetup() {
        tableViewSetup()
    }
    
    fileprivate func tableViewSetup() {
        discoveredDevicesTableView.delegate = self
        discoveredDevicesTableView.dataSource = self
        discoveredDevicesTableView.register(UINib(nibName: "DeviceTableViewCell", bundle: nil) , forCellReuseIdentifier: "discoveredDeviceCell")
        discoveredDevicesTableView.register(UINib(nibName: "DeviceStatusHeader", bundle: nil), forCellReuseIdentifier: "deviceStatusHeader")
        discoveredDevicesTableView.rowHeight = 65
        discoveredDevicesTableView.sectionHeaderHeight = 30
    }
    
    fileprivate func discoverPeripherals(withType type: LoadingHUD.LoadingType) {
        LoadingHUD.sharedInstance.loadingType = type
        LoadingHUD.sharedInstance.show()
        BluetoothManager.shared.discoverNearbyPeripherals { peripherals in
            for peripheral in peripherals {
                if !self.foundPeripherals.contains(peripheral), !self.connectedPeripherals.contains(peripheral) {
                    if peripheral.state == .connected {
                        self.connectedPeripherals.append(peripheral)
                    } else {
                        self.foundPeripherals.append(peripheral)
                    }
                }
            }
            self.discoveredDevicesTableView.reloadData()
            LoadingHUD.sharedInstance.hide()
        }
    }

}

// Action sheet methods

extension DeviceListViewController {
    
    func presentDeviceActionMenu(forSender sender: DeviceTableViewCell) {
        
        if let indexPathForCell = discoveredDevicesTableView.indexPath(for: sender) {
            
            let name = sender.deviceName ?? "Unnamed"
            let uuid = sender.uuid ?? ""
            let device : CBPeripheral!
            
            let section = tableStructure[indexPathForCell.section]
            let index = indexPathForCell.row
            
            let alertController = UIAlertController(title: "DEVICE NAME: \(name)", message: uuid, preferredStyle: .actionSheet)
            
            switch section {
                case .connectedDevices:
                    device = connectedPeripherals[index]
                    
                    alertController.addAction(UIAlertAction(title: "Disconnect", style: .default, handler: { (action) in
                        BluetoothManager.shared.disconnect(fromPeripheral: device, withUUID: CBUUID(nsuuid: device.identifier))
                        self.connectedPeripherals.remove(at: index)
                        self.discoverPeripherals(withType: .disconnecting)
                    }))
                    break
                case .availableDevices:
                    
                    if connectedPeripherals.isEmpty {
                        device = foundPeripherals[index]
                        
                        alertController.addAction(UIAlertAction(title: "Connect", style: .default, handler: { (action) in
                            BluetoothManager.shared.connect(toPeripheral: device, withUUID: CBUUID(nsuuid: device.identifier))
                            self.connectedPeripherals.append(device)
                            self.foundPeripherals.remove(at: index)
                            self.discoverPeripherals(withType: .connecting)
                        }))
                    }
                    
                    break
            }
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) in
                alertController.dismiss(animated: true, completion: nil)
            }))
            present(alertController, animated: true, completion: nil)
        }

    }
    
}
