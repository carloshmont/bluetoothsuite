//
//  HomeViewController.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/30/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet fileprivate weak var sendPingButton: UIButton!
    
    @IBOutlet fileprivate weak var controlViewPushButton: UIView!
    @IBOutlet fileprivate weak var controlViewSwitch: UIView!
    @IBOutlet fileprivate weak var controlViewSlider: UIView!
    @IBOutlet fileprivate weak var controlViewStepper: UIView!
    @IBOutlet fileprivate weak var controlViewRotation: UIView!
    
    fileprivate var isToggled : Bool = false
    fileprivate var allowTx : Bool = true
    fileprivate var sliderLastValue : Float = 0
    fileprivate var stepperLastValue : Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendPingButton.layer.cornerRadius = sendPingButton.frame.height / 2
        sendPingButton.layer.masksToBounds = true
        
        registerBluetoothNotifications()
        initialSetup()
    }
    
    deinit {
        deregisterBluetoothNotifications()
    }
    
    @IBAction func sendPingTapped(_ sender: UIButton) {
        var ledOn  : UInt8 = 110
        var ledOff : UInt8 = 0
        
        let ledOnData = Data(bytes: &ledOn, count: 1)
        let ledOffData = Data(bytes: &ledOff, count: 1)
        
        if isToggled {
            BluetoothManager.shared.write(data: ledOffData)
            isToggled = false
        } else {
            BluetoothManager.shared.write(data: ledOnData)
            isToggled = true
        }
    }
    
    @IBAction func didMoveSlider(_ sender: UISlider) {
        guard allowTx == true, sender.value != sliderLastValue else { return }
        
        allowTx = false
        
        var intValue : UInt8 = UInt8(sender.value)
        
        let _ = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { timer in
            BluetoothManager.shared.write(data: Data(bytes: &intValue, count: 1))
            
            self.allowTx = true
        }
        self.sliderLastValue = sender.value
    }
    
    @IBAction func didMoveSwitch(_ sender: UISwitch) {
        
        var ledOn  : UInt8 = 110
        var ledOff : UInt8 = 0
        
        let ledOnData = Data(bytes: &ledOn, count: 1)
        let ledOffData = Data(bytes: &ledOff, count: 1)
        
        if sender.isOn {
            BluetoothManager.shared.write(data: ledOnData)
        } else {
            BluetoothManager.shared.write(data: ledOffData)
        }
    }
    @IBAction func didTapStepper(_ sender: UIStepper) {
        var intValue : UInt8 = UInt8(sender.value)
        BluetoothManager.shared.write(data: Data(bytes: &intValue, count: 1))
    }
}

extension HomeViewController {
    
    func initialSetup() {
        
        controlViewPushButton.layer.cornerRadius = 5
        controlViewSlider.layer.cornerRadius = 5
        controlViewSwitch.layer.cornerRadius = 5
        controlViewStepper.layer.cornerRadius = 5
        controlViewRotation.layer.cornerRadius = 5
        
        controlViewSwitch.layer.masksToBounds = true
        controlViewSlider.layer.masksToBounds = true
        controlViewPushButton.layer.masksToBounds = true
        controlViewStepper.layer.masksToBounds = true
        controlViewRotation.layer.masksToBounds = true
    }
    
    func didConnectToPeripheral() {
        
        if let name = BluetoothManager.shared.getCurrentConnectedPeripheralName() {
            navigationItem.title = "\(name)"
        } else {
            navigationItem.title = "No Peripherals"
        }
    
    }
    
    func didDisconnectFromPeripheral() {
        navigationItem.title = "No Peripherals"
    }
    
    func registerBluetoothNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.didConnectToPeripheral), name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothConnectedToPeripheral), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.didDisconnectFromPeripheral), name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothDisconnectedFromPeripheral), object: nil)
    }
    
    func deregisterBluetoothNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothConnectedToPeripheral), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: BluetoothManager.Notification.BluetoothModule.bluetoothDisconnectedFromPeripheral), object: nil)
    }
    
}



