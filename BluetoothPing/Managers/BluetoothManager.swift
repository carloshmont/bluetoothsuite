//
//  BluetoothManager.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 5/17/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import Foundation
import CoreBluetooth

final class BluetoothManager : NSObject {
    
    static var shared = BluetoothManager()
    
    fileprivate var serviceUUID             : String!
    fileprivate var centralManager          : CBCentralManager!
    fileprivate var connectedPeripheral     : CBPeripheral!
    fileprivate var writableCharacteristic  : CBCharacteristic!
    
    fileprivate var discoveredPeripherals = [CBPeripheral]()
    
    fileprivate var discoveryTimer     = Timer()
    fileprivate let discoveryTimeLimit : Int = 2
    fileprivate var timerTickCount     : Int = 0
    
    private override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
    }
}

//MARK: - BluetoothManager Methods

extension BluetoothManager {
    
    func discoverNearbyPeripherals(completion: @escaping ([CBPeripheral]) -> Void){
        if centralManager.state == .poweredOn {
            centralManager.scanForPeripherals(withServices: [], options: nil)

            discoveryTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
                self.timerTickCount += 1
                if self.timerTickCount == self.discoveryTimeLimit {
                    self.timerTickCount = 0
                    self.discoveryTimer.invalidate()
                    completion(self.discoveredPeripherals)
                }
            })
            
        } else {
            completion([])
        }
    }
    
    func connect(toPeripheral peripheral: CBPeripheral, withUUID uuid: CBUUID?) {
        if centralManager.state == .poweredOn {
            let filterPeripherals = discoveredPeripherals.filter{ uuid == CBUUID.init(nsuuid: $0.identifier) }
            
            if let thisPeripheral = filterPeripherals.first, !filterPeripherals.isEmpty {
                thisPeripheral.delegate = self
                centralManager.connect(thisPeripheral, options: nil)
            } else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothFailedToFindPeripheral), object: self)
            }
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothTurnedOff), object: self)
        }
    }
    
    func disconnect(fromPeripheral peripheral: CBPeripheral, withUUID uuid: CBUUID?) {
        let filteredPeripherals = discoveredPeripherals.filter{ uuid == CBUUID.init(nsuuid: $0.identifier) }
        
        if let thisPeripheral = filteredPeripherals.first, !filteredPeripherals.isEmpty {
            centralManager.cancelPeripheralConnection(thisPeripheral)
            
        }
    }
    
    func getCurrentConnectedPeripheralName() -> String? {
        guard let name = connectedPeripheral?.name else { return nil }
        return name
    }
    
    func getCurrentConnectedPeripheralUUID() -> CBUUID? {
        guard let peripheral = connectedPeripheral else { return nil }
        return CBUUID.init(nsuuid: peripheral.identifier)
    }
    
    func write(data: Data) {
        guard let characteristic = writableCharacteristic else { return }
        connectedPeripheral.writeValue(data, for: characteristic, type: .withoutResponse)
    }
    
}

//MARK: - CBECentralManager Delegate Methods

extension BluetoothManager : CBCentralManagerDelegate {
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
            
            case .poweredOff:
                print("Bluetooth is now off")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothTurnedOff), object: self)
                break
            case .poweredOn:
                print("Bluetooth is now on")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothTurnedOn), object: self)
                break
            case .unauthorized:
                print("Bluetooth is now not authorized")
                break
            case .unknown:
                print("Bluetooth is now unknown")
                break
            case .unsupported:
                print("Bluetooth is now not supported")
                break
            case .resetting:
                print("Bluetooth is now resetting")
                break
        }
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        connectedPeripheral = peripheral
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothConnectedToPeripheral), object: self)
        peripheral.discoverServices([CBUUID(string: PeripheralUUID.service)])
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothFailedToConnectToPeripheral), object: self)
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        connectedPeripheral = nil
        writableCharacteristic = nil
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothModule.bluetoothDisconnectedFromPeripheral), object: self)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let matchingPeripherals = discoveredPeripherals.filter{ $0.identifier == peripheral.identifier }
        if matchingPeripherals.isEmpty {
            discoveredPeripherals.append(peripheral)
        }
    }
    
}

//MARK: - CBPeripheral Delegate Methods

extension BluetoothManager : CBPeripheralDelegate {
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        
        for service in services {
            if service.uuid == CBUUID(string: PeripheralUUID.service) {
                peripheral.discoverCharacteristics(nil, for: service)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else { return }
        
        for characteristic in characteristics {
            if characteristic.properties.contains(.write) || characteristic.properties.contains(.writeWithoutResponse) {
                writableCharacteristic = characteristic
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Notification.BluetoothManager.foundWritableCharacteristic), object: self)
            }
            peripheral.setNotifyValue(true, for: characteristic)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
    }
}

//MARK: - BluetoothManager Notification ID's

extension BluetoothManager {
    
    struct PeripheralUUID {
        static let service = "FFE0"
    }
    
    struct Notification {
        
        struct BluetoothManager {
            static let startedScanningForPeripherals       = "didStartScanningForPeripherals"
            static let finishedScanningForPeripherals      = "didFinishedScanningForPeripherals"
            static let foundWritableCharacteristic         = "didFoundWritableCharacteristic"
        }
        
        struct BluetoothModule {
            static let bluetoothTurnedOn                    = "didTurnedOn"
            static let bluetoothTurnedOff                   = "didTurnedOff"
            static let bluetoothIsResetting                 = "didStartResetting"
            static let bluetoothConnectedToPeripheral       = "didConnectToPeripheral"
            static let bluetoothDisconnectedFromPeripheral  = "didDisconnectFromPeripheral"
            static let bluetoothFailedToConnectToPeripheral = "didFailToConnectToPeripheral"
            static let bluetoothFailedToFindPeripheral      = "didFailToFindPeripheral"
        }
    }
}
