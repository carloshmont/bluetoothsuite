//
//  NotificationManager.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/30/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import Foundation
import UserNotifications

class NotificationManager {
    
    static let shared = NotificationManager()
    
    fileprivate let center = UNUserNotificationCenter.current()
    fileprivate let options: UNAuthorizationOptions = [.alert, .sound]
    fileprivate let timeUntilNotification : Double = 5
    
    private init() {
        
    }
    
    func requestAuthorizationForNotifications() {
        
        center.requestAuthorization(options: options) {
            (granted, error) in
            if !granted {
                print("ERROR - Something went wrong.")
            } else {
                print("SUCCESS - Notification permission is granted.")
            }
        }
        
    }
    
    func sendNotification(withTitle title: String, andBody body: String) {
        let content = UNMutableNotificationContent()
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeUntilNotification, repeats: false)
        let identifier = "UYLLocalNotification"
        
        content.title = title
        content.body = body
        content.sound = UNNotificationSound.default()
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print(error.localizedDescription)
            }
        })
    }
    
}
