//
//  DeviceStatusHeader.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/29/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit

class DeviceStatusHeader: UITableViewCell {

    @IBOutlet fileprivate weak var deviceStatusLabel: UILabel!
    
    var deviceStatus : String! {
        didSet{
            deviceStatusLabel.text = deviceStatus
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }    
}
