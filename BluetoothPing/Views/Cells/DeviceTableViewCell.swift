//
//  DeviceTableViewCell.swift
//  BluetoothPing
//
//  Created by Carlos H Montenegro on 3/27/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit

protocol DeviceTableViewCellDelegate : class {
    func didSetSlider(state: Bool, in cell: DeviceTableViewCell)
}

class DeviceTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var discoveredDeviceNameLabel: UILabel!
    @IBOutlet fileprivate weak var connectSliderSwitch: UISwitch!
    @IBOutlet fileprivate weak var uuidDeviceLabel: UILabel!
    
    var delegate : DeviceTableViewCellDelegate!
    
    var hideSlider : Bool! {
        didSet{
            connectSliderSwitch.isHidden = hideSlider
        }
    }
    
    var deviceName: String! {
        didSet{
            if let thisDeviceName = deviceName {
                discoveredDeviceNameLabel.text = thisDeviceName
            } else {
                discoveredDeviceNameLabel.text = "UNKNOWN"
            }
        }
    }
    
    var uuid : String! {
        didSet{
            uuidDeviceLabel.text = uuid
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if let delegateRef = delegate {
            delegateRef.didSetSlider(state: sender.isOn, in: self)
        }
    }

}
