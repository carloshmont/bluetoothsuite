//
//  LoadingHUD.swift
//
//  Created by Carlos H Montenegro on 1/24/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import Foundation
import UIKit

class LoadingHUD {
    
    enum LoadingType : String{
        case discovery     = "Finding"
        case connecting    = "Connecting"
        case disconnecting = "Disconnecting"
    }
    
    static let sharedInstance = LoadingHUD()
    
    fileprivate var loadingHUDView : UIView?
    
    //Colors Setup
    
    fileprivate let hudColor        : UIColor = UIColor.white
    fileprivate let spinnerColor    : UIColor = UIColor.gray
    fileprivate let labelColor      : UIColor = UIColor.gray
    fileprivate let backgroundColor : UIColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    fileprivate var label           : UILabel = UILabel()
    
    //Label Text
    
    var loadingType : LoadingType = .discovery {
        didSet {
            label.text = loadingType.rawValue
        }
    }
    
    //Label Font
    fileprivate let textFont = UIFont(name: "Gill Sans", size: 15)
    
    private init() {
        initialSetup()
    }
    
}

extension LoadingHUD {
    func show() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.loadingHUDView?.alpha = 1
        }, completion: nil)
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.loadingHUDView?.alpha = 0
        }, completion: nil)
    }
}

extension LoadingHUD {
    
    fileprivate func initialSetup() {

        loadingHUDView = fullViewSetup()
        
        UIApplication.shared.keyWindow?.addSubview(loadingHUDView!)
        
        NSLayoutConstraint(item: loadingHUDView!, attribute: .top, relatedBy: .equal, toItem: UIApplication.shared.keyWindow!, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: loadingHUDView!, attribute: .bottom, relatedBy: .equal, toItem: UIApplication.shared.keyWindow!, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: loadingHUDView!, attribute: .left, relatedBy: .equal, toItem: UIApplication.shared.keyWindow!, attribute: .left, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: loadingHUDView!, attribute: .right, relatedBy: .equal, toItem: UIApplication.shared.keyWindow!, attribute: .right, multiplier: 1, constant: 0).isActive = true
        
        loadingHUDView?.alpha = 0
    }
    
    fileprivate func fullViewSetup() -> UIView {
        let mainView = backgroundView()
        let container = spinnerContainerView()
        
        let spinner = spinnerView()
        let label = spinnerLabel()
        let stackView = UIStackView(arrangedSubviews: [spinner, label])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        container.addSubview(stackView)

        NSLayoutConstraint(item: stackView, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: stackView, attribute: .centerY, relatedBy: .equal, toItem: container, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        
        mainView.addSubview(container)
        
        NSLayoutConstraint(item: container, attribute: .centerX, relatedBy: .equal, toItem: mainView, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: container, attribute: .centerY, relatedBy: .equal, toItem: mainView, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
        
        return mainView
    }
    
    fileprivate func spinnerLabel() -> UILabel {
        label.text = loadingType.rawValue
        label.textColor = labelColor
        label.font = textFont
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }
    
    fileprivate func spinnerView() -> UIActivityIndicatorView {
        let spinnerView = UIActivityIndicatorView()
        spinnerView.isHidden = false
        spinnerView.activityIndicatorViewStyle = .whiteLarge
        spinnerView.startAnimating()
        spinnerView.color = spinnerColor
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        return spinnerView
    }
    
    fileprivate func spinnerContainerView() -> UIView {
        let backgroundView = UIView()
        backgroundView.backgroundColor = hudColor
        backgroundView.layer.cornerRadius = 10
        backgroundView.layer.masksToBounds = true
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: backgroundView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100).isActive = true
        NSLayoutConstraint(item: backgroundView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100).isActive = true
        
        return backgroundView
    }
    
    fileprivate func backgroundView() -> UIView {
        let backgroundView = UIView()
        backgroundView.backgroundColor = backgroundColor
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        
        return backgroundView
    }
    
}
