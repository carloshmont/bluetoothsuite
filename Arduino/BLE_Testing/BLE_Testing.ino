#include<SoftwareSerial.h>

SoftwareSerial BT1(3,2); // TX, RX 

int ledPin = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledPin,OUTPUT);
  
  Serial.begin(9600);
  //Serial.println("Enter AT commands:");
  //BT1.begin(9600);
}

void loop() {

//  if (BT1.available()) {
//    //Serial.write(BT1.read());
//    int i = (int)BT1.read();
//    setIntensity(i);
//  }

  if(Serial.available()) {
    int i = (int)Serial.read();
    setIntensity(i);
  }
}

void setIntensity(int value) {
  if (value >= 0 && value <= 110) {
    analogWrite(ledPin, value);
  }
}

