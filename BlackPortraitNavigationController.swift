//
//  BlackPortraitNavigationController.swift
//  InFlixTesting
//
//  Created by Carlos H Montenegro on 1/20/17.
//  Copyright © 2017 Carlos H Montenegro. All rights reserved.
//

import UIKit

@IBDesignable
class BlackPortraitNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
}

extension BlackPortraitNavigationController {
    
    fileprivate func initialSetup() {
        navigationBar.barTintColor = UIColor(red: 41/255, green: 187/255, blue: 156/255, alpha: 1)
        UIApplication.shared.statusBarStyle = .lightContent
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override var shouldAutorotate: Bool {
        get{
            return false
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get{
            return .portrait
        }
    }
    
}
